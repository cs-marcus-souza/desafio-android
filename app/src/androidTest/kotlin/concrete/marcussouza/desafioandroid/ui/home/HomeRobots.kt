package concrete.marcussouza.desafioandroid.ui.home

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import br.com.concretesolutions.kappuccino.actions.ClickActions.click
import br.com.concretesolutions.kappuccino.assertions.VisibilityAssertions.displayed
import br.com.concretesolutions.kappuccino.custom.recyclerView.RecyclerViewInteractions.recyclerView
import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import concrete.marcussouza.desafioandroid.R


class HomeRobots(var rule: ActivityTestRule<HomeActivity>, val serverRule: RequestMatcherRule) {

    fun initActivity(intent: Intent) {
        rule.launchActivity(intent)
    }

    fun fetchRepoList(order: Int) {
        serverRule
                .addFixture("repo_list.json")
                .ifRequestMatches()
                .methodIs(HttpMethod.GET)
                .orderIs(order)
                .pathIs("/search/repositories")
    }

    fun validateToolbarTitle(title: String) {
        displayed {
            parent(R.id.activity_home_toolbar) {
                text(title)
            }
        }
    }

    fun fetchRepoListError(order: Int) {
        serverRule
                .addFixture("repo_list_error.json")
                .ifRequestMatches()
                .methodIs(HttpMethod.GET)
                .orderIs(order)
                .pathIs("/search/repositories")
    }

    fun validateSnackBarShow() {
        displayed {
            rule.activity.snackBarError
        }
    }

    fun mustContainItens(total: Int) {
        recyclerView(R.id.activity_home_recycler_view) {
            sizeIs(total)
        }
    }

    fun clickRetry() {
        Thread.sleep(1000)
        click {
            id(android.support.design.R.id.snackbar_action)
        }
    }
}