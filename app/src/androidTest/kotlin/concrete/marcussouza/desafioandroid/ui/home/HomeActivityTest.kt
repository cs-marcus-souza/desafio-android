package concrete.marcussouza.desafioandroid.ui.home

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.concretesolutions.requestmatcher.InstrumentedTestRequestMatcherRule
import concrete.marcussouza.desafioandroid.TestServerUrl
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<HomeActivity>(HomeActivity::class.java, false, false)

    @Rule
    @JvmField
    val serverRule = InstrumentedTestRequestMatcherRule()

    private fun robots(func: HomeRobots.() -> Unit) = HomeRobots(rule, serverRule).apply(func)

    private val intent = Intent()

    @Before
    fun setup() {
        TestServerUrl.url = serverRule.url("/")
    }

    @Test
    fun mustHaveToolbarTitle() {
        robots {
            fetchRepoList(1)
            initActivity(intent)
            validateToolbarTitle("JavaGit Repo")
        }
    }

    @Test
    fun mustLoadRepoListAndContain10Items() {
        robots {
            fetchRepoList(1)
            initActivity(intent)
            mustContainItens(10)
        }
    }

    @Test
    fun mustLoadRepoErrorAndShowSnackBar() {
        robots {
            fetchRepoListError(1)
            initActivity(intent)
            validateSnackBarShow()
        }
    }

    @Test
    fun mustLoadRepoListOnRetryAfterError() {
        robots {
            fetchRepoListError(1)
            fetchRepoList(2)
            initActivity(intent)
            validateSnackBarShow()
            clickRetry()
            mustContainItens(10)
        }
    }
}