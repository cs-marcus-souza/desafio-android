package concrete.marcussouza.desafioandroid.di

import com.squareup.moshi.Moshi
import concrete.marcussouza.desafioandroid.BuildConfig
import concrete.marcussouza.desafioandroid.data.api.DesafioApi
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module(includes = arrayOf(ViewModelModule::class))
open class AppModule {

    @Provides
    @Singleton
    fun provideMoshi() = Moshi.Builder().build()

    @Provides
    @Singleton
    open fun provideOkHttpClientBuilder(interceptors: Set<@JvmSuppressWildcards Interceptor>): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
        interceptors.forEach { builder.addInterceptor(it) }
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        return builder
    }

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient, moshi: Moshi, @Named("HTTP_URL") url: String): DesafioApi {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()
                .create(DesafioApi::class.java)
    }
}