package concrete.marcussouza.desafioandroid.di

import concrete.marcussouza.desafioandroid.ui.detail.DetailActivity
import concrete.marcussouza.desafioandroid.ui.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun provideHomeActivityInjector(): HomeActivity

    @ContributesAndroidInjector
    abstract fun provideDetailActivityInjector(): DetailActivity
}