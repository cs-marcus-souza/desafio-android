package concrete.marcussouza.desafioandroid.di

import concrete.marcussouza.desafioandroid.DesafioApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        ActivitiesModule::class,
        NetworkModule::class
))
interface AppComponent {

    fun inject(app: DesafioApp)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(app: DesafioApp): Builder
    }
}