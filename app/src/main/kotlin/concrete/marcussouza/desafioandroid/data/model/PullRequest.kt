package concrete.marcussouza.desafioandroid.data.model

import com.squareup.moshi.Json
import java.util.*


data class PullRequest(
        @field:Json(name = "html_url") val htmlUrl: String,
        @field:Json(name = "user") val user: Owner,
        @field:Json(name = "body") val body: String,
        @field:Json(name = "created_at") val createdAt: Date,
        @field:Json(name = "name") val name: String
)