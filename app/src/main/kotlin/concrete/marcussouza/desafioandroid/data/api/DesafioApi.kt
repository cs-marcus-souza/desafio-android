package concrete.marcussouza.desafioandroid.data.api

import concrete.marcussouza.desafioandroid.data.model.PullRequest
import concrete.marcussouza.desafioandroid.data.model.Repository
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface DesafioApi {

    @GET("search/repositories?q=language:Java&sort=stars&per_page=10")
    fun fetchRepositorieList(@Query("page") page: Int): Observable<Repository>

    @GET("/repos/{login}/{repo}/pulls?per_page=10")
    fun fetchPullRequestList(@Path("login") login: String, @Path("repo") repo: String, @Query("page") page: Int): Observable<PullRequest>
}