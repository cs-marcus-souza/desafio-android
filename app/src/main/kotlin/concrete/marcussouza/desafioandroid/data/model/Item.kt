package concrete.marcussouza.desafioandroid.data.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json


data class Item(
        @field:Json(name = "id") val id: Int,
        @field:Json(name = "name") val name: String,
        @field:Json(name = "full_name") val fullName: String,
        @field:Json(name = "description") val description: String,
        @field:Json(name = "owner") val owner: Owner,
        @field:Json(name = "stargazers_count") val stargazersCount: Int,
        @field:Json(name = "forks_count") val forksCount: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Owner::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(fullName)
        parcel.writeString(description)
        parcel.writeParcelable(owner, flags)
        parcel.writeInt(stargazersCount)
        parcel.writeInt(forksCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }
}