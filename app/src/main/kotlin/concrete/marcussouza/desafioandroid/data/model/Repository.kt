package concrete.marcussouza.desafioandroid.data.model

import com.squareup.moshi.Json


data class Repository(
        @field:Json(name = "items") var itemList: List<Item>
)