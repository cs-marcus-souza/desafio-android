package concrete.marcussouza.desafioandroid.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import concrete.marcussouza.desafioandroid.R
import concrete.marcussouza.desafioandroid.data.model.Item
import concrete.marcussouza.desafioandroid.ui.BaseActivity
import concrete.marcussouza.desafioandroid.ui.BaseRecyclerAdapter
import concrete.marcussouza.desafioandroid.ui.custom.InfiniteScrollListener
import concrete.marcussouza.desafioandroid.ui.detail.DetailActivity
import concrete.marcussouza.desafioandroid.util.bindView
import java.util.*
import javax.inject.Inject


class HomeActivity : BaseActivity(), BaseRecyclerAdapter.OnItemClickListener, View.OnClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(HomeViewModel::class.java)
    }

    private val homeListAdapter by lazy { HomeRepositorieListAdapter() }

    private val toolBar: Toolbar by bindView(R.id.activity_home_toolbar)
    private val progressBar: ProgressBar by bindView(R.id.activity_home_progress)
    private val recyclerView: RecyclerView by bindView(R.id.activity_home_recycler_view)
    private val container: ConstraintLayout by bindView(R.id.activity_home_container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initObservers()
        setupToolbar()
        setupRecyclerView()

        if (viewModel.repositorieResult.value == null) {
            viewModel.incrementCurrentPage()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolBar)
        supportActionBar?.title = resources.getString(R.string.app_name)
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(recyclerView.context)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, layoutManager.orientation)

        val infinitScroll = object : InfiniteScrollListener(layoutManager) {
            override fun isLoading(): Boolean {
                return viewModel.loading.value!!
            }

            override fun loadMoreItems() {
                loadMore()
            }
        }

        homeListAdapter.listener = this
        recyclerView.adapter = homeListAdapter
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.addOnScrollListener(infinitScroll)
        recyclerView.layoutManager = layoutManager
    }

    private fun initObservers() {
        viewModel.repositorieResult.observeResource(this, onSuccess = {
            includeItems(it.itemList)
        }, onError = {
            showSnackBarError(container, this)
            viewModel.decreaseCurrentPage()
        })

        viewModel.loading.observe(this, Observer {
            progressBar.visibility = if (it!!) View.VISIBLE else View.GONE
        })

        viewModel.currentPageResult.observe(this, Observer {
            viewModel.fetchRepositorieList()
        })
    }

    private fun includeItems(itemList: List<Item>) {
        viewModel.loading.postValue(false)
        homeListAdapter.addToList(itemList as ArrayList<Item>)
    }

    private fun loadMore() {
        viewModel.loading.postValue(true)
        viewModel.incrementCurrentPage()
    }

    override fun onItemClick(view: View, position: Int) {
        val item = homeListAdapter.getItem(position)
        startDetailActivity(item)
    }

    private fun startDetailActivity(item: Item) {
        val intent = Intent(applicationContext, DetailActivity::class.java)
        intent.putExtra("item", item)
        startActivity(intent)
    }

    override fun onClick(view: View?) {
        if (view?.id == android.support.design.R.id.snackbar_action) {
            viewModel.incrementCurrentPage()
        }
    }
}