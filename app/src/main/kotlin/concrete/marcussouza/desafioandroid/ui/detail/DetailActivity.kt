package concrete.marcussouza.desafioandroid.ui.detail

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.Toolbar
import concrete.marcussouza.desafioandroid.R
import concrete.marcussouza.desafioandroid.data.model.Item
import concrete.marcussouza.desafioandroid.ui.BaseActivity
import concrete.marcussouza.desafioandroid.util.bindView
import javax.inject.Inject


class DetailActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val toolBar: Toolbar by bindView(R.id.activity_detail_toolbar)

    lateinit var item: Item

    val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        item = intent.getParcelableExtra("item")

        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolBar)
        supportActionBar?.title = item.fullName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}