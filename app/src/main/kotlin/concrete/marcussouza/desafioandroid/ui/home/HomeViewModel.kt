package concrete.marcussouza.desafioandroid.ui.home

import android.arch.lifecycle.MutableLiveData
import concrete.marcussouza.desafioandroid.data.api.DesafioApi
import concrete.marcussouza.desafioandroid.data.model.Item
import concrete.marcussouza.desafioandroid.data.model.Repository
import concrete.marcussouza.desafioandroid.ui.BaseViewModel
import concrete.marcussouza.desafioandroid.util.ResourceLiveData
import concrete.marcussouza.desafioandroid.util.toErrorHandler
import javax.inject.Inject


class HomeViewModel @Inject constructor(api: DesafioApi) : BaseViewModel(api) {

    val repositorieResult = ResourceLiveData<Repository>()
    val currentPageResult = MutableLiveData<Int>()
    var currentPage: Int = 0
    var lastPage: Int = 0

    var localList = MutableLiveData<ArrayList<Item>>()

    init {
        localList.value = ArrayList()
        loading.value = false
    }

    fun fetchRepositorieList() {
        if (lastPage != currentPage) {
            lastPage = currentPage

            api.fetchRepositorieList(currentPage)
                    .doOnSubscribe { loading.postValue(true) }
                    .doFinally { loading.postValue(false) }
                    .map { repo ->
                        localList.value?.addAll(repo.itemList)
                        repo.itemList = localList.value!!
                        return@map repo
                    }
                    .toErrorHandler()
                    .subscribeLiveData(this, repositorieResult)
        }
    }

    fun incrementCurrentPage() {
        currentPage += 1
        currentPageResult.postValue(currentPage)
    }

    fun decreaseCurrentPage() {
        currentPage -= 1
        lastPage = currentPage
    }
}