package concrete.marcussouza.desafioandroid.ui

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import concrete.marcussouza.desafioandroid.R
import dagger.android.AndroidInjection


abstract class BaseActivity : AppCompatActivity(), LifecycleRegistryOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    override fun getLifecycle() = lifecycleRegistry

    lateinit var snackBarError: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun showSnackBarError(view: View, listener: View.OnClickListener) {
        snackBarError = Snackbar.make(view, R.string.error_title, Snackbar.LENGTH_INDEFINITE)
        snackBarError.setAction(R.string.error_retry_btn, listener)
        snackBarError.show()
    }
}