package concrete.marcussouza.desafioandroid.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import concrete.marcussouza.desafioandroid.data.api.DesafioApi
import io.reactivex.disposables.CompositeDisposable


abstract class BaseViewModel(val api: DesafioApi) : ViewModel() {

    var disposables = CompositeDisposable()

    val loading = MutableLiveData<Boolean>()

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}