package concrete.marcussouza.desafioandroid.ui.home

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import concrete.marcussouza.desafioandroid.R
import concrete.marcussouza.desafioandroid.data.model.Item
import concrete.marcussouza.desafioandroid.ui.BaseRecyclerAdapter
import de.hdodenhof.circleimageview.CircleImageView


class HomeRepositorieListAdapter() : BaseRecyclerAdapter<Item>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ViewHolder).bind(recyclerList!![position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_repositorie, parent, false)
        return ViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return if (recyclerList != null) recyclerList!!.size else 0
    }

    class ViewHolder(val view: View, val listener: OnItemClickListener?) : RecyclerView.ViewHolder(view), View.OnClickListener {

        val container: ConstraintLayout = view.findViewById(R.id.item_repositorie_container)
        val name: AppCompatTextView = view.findViewById(R.id.item_repositorie_name)
        val description: AppCompatTextView = view.findViewById(R.id.item_repositorie_description)
        val forkCount: AppCompatTextView = view.findViewById(R.id.item_repositorie_fork_count)
        val starCount: AppCompatTextView = view.findViewById(R.id.item_repositorie_star_count)
        val profileImage: CircleImageView = view.findViewById(R.id.item_repositorie_profile_image)
        val profileName: AppCompatTextView = view.findViewById(R.id.item_repositorie_profile_name)

        init {
            container.setOnClickListener(this)
        }

        fun bind(item: Item) {
            name.text = item.fullName
            description.text = item.description
            forkCount.text = item.forksCount.toString()
            starCount.text = item.stargazersCount.toString()
            Picasso
                    .with(view.context)
                    .load(item.owner.avatarUrl)
                    .placeholder(R.drawable.vector_profile)
                    .into(profileImage)
            profileName.text = item.owner.login
        }

        override fun onClick(view: View?) {
            listener!!.onItemClick(view!!, adapterPosition)
        }
    }
}