package concrete.marcussouza.desafioandroid

import android.app.Activity
import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import concrete.marcussouza.desafioandroid.di.AppInjector
import concrete.marcussouza.desafioandroid.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


open class DesafioApp : Application(), HasActivityInjector {
    @Inject lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        AppInjector.init(this, createAppComponent())
    }

    open fun createAppComponent() = DaggerAppComponent.builder()
            .application(this)
            .build()

    override fun activityInjector() = activityInjector
}